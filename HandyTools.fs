﻿namespace HandyTools

open System

[<AutoOpen>]
module General = 
    let inline idprintn x = printfn "%O" x; x
    let inline idstrprint text x = printfn "%s" text; x
    let inline idprintnList x = printfn "%A" x

    let readln = Console.ReadLine
    let readInt = readln >> Int32.Parse
    let readInt64 = readln >> Int64.Parse
    let readFloat = readln >> Double.Parse
    let readFloat32 = readln >> Single.Parse
    let readDecimal = readln >> Decimal.Parse

    let inline toStr x = x.ToString()

    let push2 f (a, b) =
        a, f b
    let push1 f (a, b) =
        f a, b 

    let (&+&) a b =
        a.ToString() + b.ToString()

module Arrays =
    let rec shiftSlice (arr : _[]) f t =
        match arr.Length with
        | l when f < 0 && t < l ->
            let prev = shiftSlice (arr) (f + arr.Length) (arr.Length - 1)
            prev |> Array.append (arr.[0..t])
        | l when f >= 0 && t >= l ->
            let prev = arr.[f..]
            let next = shiftSlice arr 0 (t - arr.Length)
            prev |> Array.append next
        | l when f < 0 && t >= l ->
            let prev = shiftSlice (arr) (f + arr.Length) (arr.Length - 1)
            let next = shiftSlice arr 0 (t - arr.Length)
            prev |> Array.append next
        | _ -> arr.[f..t]

module Func =
    let rec Ycomb f x = f (Ycomb f) x
    let argS2 f x y = f y x
    let argS3 f x y z = f z x y
    let argS4 f x y z w = f w x y z
    let condt f a b = if f() then a else b
    let hideA0 f = fun () -> f
    let hideA1 f a = fun () -> f a
    let hideA2 f a b = fun () -> f a b
    let hideA3 f a b c = fun () -> f a b c
    let hideA4 f a b c d = fun () -> f a b c d
    let accept2 f (a, b) = f a b
    let accept3 f (a, b, c) = f a b c
    let accept4 f (a, b, c, d) = f a b c d
    let trySome f =
        try
            f()
        with
        | _ -> None

    let opArg op f =
        fun x y ->
            op (f x) (f y)
    let opCompose op (a, b) =
        fun x y ->
            op (a x) (b y)
    
    let (.>>.) a b = opArg b a

module Integer =
    open Func

    let rec inField m a =
        match a with
        | _ when a < 0 -> inField m (a + m)
        | _ when a >= m -> a % m
        | _ -> a

    let modSum m a = (inField m .>>. (+)) a >> inField m
    let modSub m a = (inField m .>>. (-)) a >> inField m
    let modMul m a = (inField m .>>. (*)) a >> inField m

module Math =
    let inline fabs x = if x < 0.0 then -x else x
    let inline fsign x = if x < 0.0 then -1.0 elif x = 0.0 then 0.0 else 1.0
    let inline inSeg (a, b) x = a <= x && x <= b
    let inline inRange (a, b) x = a < x && x < b 

    type System.Double with
        member n.InSeg(a, b) = n |> inSeg (a, b)
        member n.InRange(a, b) = n |> inRange (a, b)
        member n.Abs() = Math.Abs n

open System.IO
open System.IO.Compression
module FileSystem =
    let (<<-) (stream : TextWriter) (o) =
        stream.Write (o.ToString())
        stream
    
    let inline (/>) (s1 : string) (s2 : string) =
        Path.Combine(s1, s2)
    
    let inline (/!>) (s1 : string) (s2 : string) =
        if Directory.Exists s1 |> not then
            Directory.CreateDirectory s1 |> ignore
        s1 /> s2  

    let inline (<-&) (file) (o) =
        File.AppendAllText(file, o.ToString())
        file

    let inline (<-&&) (file) (o) =
        File.WriteAllText(file, o.ToString())
        file

    let inline openText file =
        File.OpenText file

    let inline openWrite file =
        File.OpenWrite

    let inline openFile file mode =
        File.Open(file, mode)

    let inline readAllLines file =
        File.ReadAllLines file

    let inline writeAllLines file lines =
        File.WriteAllLines(file, lines)

    let inline appendAllLines file lines = 
        File.AppendAllLines(file, lines)

    let inline readAllText file =
        File.ReadAllText file

    let inline writeAllText file text =
        File.WriteAllText(file, text)

    let inline appendAllText file text =
        File.AppendAllText(file, text)

    let inline exists str =
        Directory.Exists str || File.Exists str

    let inline getDirs dir =
        Directory.GetDirectories dir
    
    let inline getFiles dir =
        Directory.GetFiles dir

    let inline enumDirs dir =
        Directory.EnumerateDirectories dir

    let inline enumFiles dir =
        Directory.EnumerateFiles dir

    let zipDir dir (archive) =
        ZipFile.CreateFromDirectory(dir, archive)

    let (|><|) (archive : ZipArchive) (file, entry)  =
        archive.CreateEntryFromFile(file, entry)

    let makeArchive name =
        new ZipArchive(File.Create name)

    let unzip (archive : ZipArchive) dest =
        archive.ExtractToDirectory(dest)

    let existsThen f th =
        if exists th then Some(f th) else None
    